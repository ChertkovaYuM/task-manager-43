package ru.tsc.chertkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.repository.model.IUserRepository;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<User>
        implements IUserRepository {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull User model) {
        super.add(model);
    }

    @Override
    public void update(@NotNull User model) {
        super.update(model);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull final String email) {
        return entityManager.find(User.class, email);
    }

    @Override
    @Nullable
    public User findByLogin(@NotNull final String login) {
        return entityManager.find(User.class, login);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        entityManager.createQuery("DELETE FROM User e WHERE e.login=:login")
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public int isLoginExist(@NotNull final String login) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM User e WHERE e.login=:login", Integer.class)
                .setParameter("login", login)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public int isEmailExist(@NotNull final String email) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM User e WHERE e.email=:email", Integer.class)
                .setParameter("email", email)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void changeRole(@NotNull final String id,
                           @NotNull final Role role) {
        entityManager.createQuery("UPDATE User e SET e.role=:role WHERE e.id=:id")
                .setParameter("role", role)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public int existsById(@NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM User e WHERE e.id=:id", Integer.class)
                .setParameter("id", id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void setPassword(@NotNull final String id,
                            @NotNull final String passwordHash) {
        entityManager.createQuery("UPDATE User e SET e.password=:passwordHash WHERE e.id=:id")
                .setParameter("passwordHash", passwordHash)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void setLockedFlag(@NotNull final String login,
                              @NotNull final Boolean locked) {
        entityManager.createQuery("UPDATE User e SET e.locked=:locked WHERE e.login=:login")
                .setParameter("locked", locked)
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM User")
                .executeUpdate();
    }

    @Override
    @NotNull
    public List<User> findAll() {
        return entityManager.createQuery("FROM User", User.class).getResultList();
    }

    @Override
    @Nullable
    public User findById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM User e", Integer.class)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.remove(entityManager.getReference(User.class, id));
    }

}
