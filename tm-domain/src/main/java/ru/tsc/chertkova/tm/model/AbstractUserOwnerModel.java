package ru.tsc.chertkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnerModel extends AbstractModel {

    @Nullable
    @ManyToOne
    protected User user;

    public String getUserId() {
        return user.getId();
    }

    public void setUserId(String Id) {
        user.setId(Id);
    }

}
