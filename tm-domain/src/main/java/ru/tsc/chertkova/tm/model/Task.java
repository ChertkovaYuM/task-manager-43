package ru.tsc.chertkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.model.IWBS;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.util.DateUtil;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public class Task extends AbstractFieldsModel implements IWBS {

    private static final long serialVersionUID = 1;

    @Nullable
    @ManyToOne
    private Project project;

    public Task(@NotNull final String name,
                @NotNull final Status status,
                @Nullable final Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public Task(@NotNull final String name,
                @NotNull final String description,
                @Nullable final String projectId) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return getId() + " - " + name + " : " +
                description + ", " + status + ", " + DateUtil.toString(dateBegin);
    }

    public String getProjectId() {
        return project.getId();
    }

}
