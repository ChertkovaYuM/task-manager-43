package ru.tsc.chertkova.tm.dto.response;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.dto.response.AbstractResultResponse;

public final class ApplicationErrorResponse extends AbstractResultResponse {

    public ApplicationErrorResponse() {
        setSuccess(false);
    }

    public ApplicationErrorResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
