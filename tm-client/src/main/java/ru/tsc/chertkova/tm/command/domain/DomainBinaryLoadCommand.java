package ru.tsc.chertkova.tm.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.Domain;
import ru.tsc.chertkova.tm.dto.request.data.DataBinaryLoadRequest;
import ru.tsc.chertkova.tm.enumerated.Role;

import java.io.*;

public final class DomainBinaryLoadCommand extends AbstractDomainCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    public static final String DESCRIPTION = "Load date from binary file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD BINARY]");
        getServiceLocator().getDomainEndpoint().loadDataBinary(new DataBinaryLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
